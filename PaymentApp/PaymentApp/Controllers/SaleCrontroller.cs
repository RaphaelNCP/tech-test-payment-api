﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PaymentApp.Context;
using PaymentApp.Models;

namespace PaymentApp.Controllers;
[Route("api/[controller]")]
[ApiController]
public class SaleCrontroller(PaymentContext context) : ControllerBase
{
    private readonly PaymentContext _context = context;

    [HttpPost]
    public IActionResult RegisterSale(Sale sale)
    {
        _context.Sales.Add(sale);
        _context.SaveChanges();

        return CreatedAtAction(nameof(GetSaleById), new { id = sale.Id }, sale);
    }

    [HttpGet("{id}")]
    public IActionResult GetSaleById(int id)
    {
        Sale sale = _context.Sales.Find(id);

        if (sale == null) return NotFound();

        return Ok(sale);
    }

    [HttpPut("{id}")]
    public IActionResult PutSale(int id, Sale sale)
    {
        Sale currentSale = _context.Sales.Find(id);
        if (currentSale == null) return NotFound();

        currentSale.Seller = sale.Seller;
        currentSale.Order = sale.Order;
        currentSale.Date = sale.Date;
        currentSale.Status = sale.Status;
        _context.SaveChanges();

        return Ok(currentSale);
    }
}
