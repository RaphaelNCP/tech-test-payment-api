﻿namespace PaymentApp.Models;

public class Sale
{
    public int Id { get; set; }
    public Seller Seller { get; set; }
    public DateTime Date { get; set; } 
    public Order Order { get; set; } 
    public StatusEnum Status { get; set; } = StatusEnum.AwaitingPayment;
}
