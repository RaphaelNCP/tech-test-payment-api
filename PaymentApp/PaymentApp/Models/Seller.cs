﻿namespace PaymentApp.Models;

public class Seller
{
    public int Id { get; set; }
    public string Cpf { get; set; } = String.Empty;
    public string Name { get; set; } = String.Empty;
    public string Email { get; set; } = String.Empty;
    public string CellPhone { get; set; } = String.Empty;

}
