﻿namespace PaymentApp.Models;

public enum StatusEnum
{
    PaymentAccept,
    SentToCarrier,
    Delivered,
    Canceled,
    AwaitingPayment
}
