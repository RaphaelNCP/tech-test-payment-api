﻿using Microsoft.EntityFrameworkCore;
using PaymentApp.Models;

namespace PaymentApp.Context;

public class PaymentContext(DbContextOptions<PaymentContext> options) : DbContext(options)
{
    public DbSet<Sale> Sales { get; set; }
    public DbSet<Seller> sallers { get; set; }
    public DbSet<Order> orders { get; set; }
    public DbSet<item> items { get; set; }
}
